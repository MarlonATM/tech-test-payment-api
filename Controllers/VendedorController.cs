using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly VendaContext _context;
        public VendedorController(VendaContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Criar(Vendedor vendedor)
        {
            if (vendedor.Nome == null || vendedor.Cpf == null || vendedor.Email == null || vendedor.Telefone == null)
            {
                return BadRequest(new { Erro = "Os dados do vendedor não podem estar vazios." });
            }

            _context.Add(vendedor);
            _context.SaveChanges();
            return Ok(vendedor);
        }

        [HttpGet("ObterTodos")]
        public IActionResult ObterTodos()
        {
            var vendedor = _context.Vendedores;
            return Ok(vendedor);
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var vendedorBanco = _context.Vendedores.Find(id);

            if (vendedorBanco == null)
            {
                return NotFound();
            }

            _context.Remove(vendedorBanco);
            _context.SaveChanges();
            return Ok(vendedorBanco);
        }
    }
}