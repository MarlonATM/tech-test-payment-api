using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")] 
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;
        public VendaController(VendaContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Criar(Venda venda)
        {
            if (String.IsNullOrEmpty(venda.ItensVendidos))
            {
                return BadRequest(new { Erro = "O campo de Itens vendidos não pode ser vazio ou nulo." });
            }

            _context.Add(venda);
            _context.SaveChanges();
            return Ok(venda);          
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);

            if (venda == null)
            {
                return NotFound();
            }

            return Ok(venda);
        }

        [HttpPut("{id}")]
        public IActionResult AtualizarStatus(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
            {
                return NotFound();
            }

            vendaBanco.Status = venda.Status;

            _context.Update(vendaBanco);
            _context.SaveChanges();
            return Ok(vendaBanco);
        }
    }
}